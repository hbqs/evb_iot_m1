//硬件驱动
#include "oled.h"
#include "i2c.h"
#include "main.h"
#include "delay.h"
//字库
#include "oled_zk.h"

//C库
#include <stdarg.h>
#include <stdio.h>

/*
************************************************************
*	函数名称：	oled_send_byte
*
*	函数功能：	OLED发送一个字节
*
*	入口参数：	byte：需要发送的字节
*
*	返回参数：	无
*
*	说明：		
************************************************************
*/
void oled_send_byte(unsigned char byte)
{

	unsigned char i = 0;
	
	for(; i < 8; i++)		
	{
		if(byte & 0x80)
			SDA_H;
		else
			SDA_L;
		
		HAL_Delay_us(iicInfo.speed);
		
		SCL_H;
		HAL_Delay_us(iicInfo.speed);
		SCL_L;
		
		byte <<= 1;
	}

}

/*
************************************************************
*	函数名称：	oled_write_data
*
*	函数功能：	OLED写入一个数据
*
*	入口参数：	byte：需要写入的数据
*
*	返回参数：	写入结果
*
*	说明：		0-成功		1-失败
************************************************************
*/
_Bool oled_write_data(unsigned char byte)
{
	
	i2c_start();
	
	oled_send_byte(OLED_ADDRESS);
	if(i2c_wait_ack(5000))	//等待应答
		return 1;
	
	oled_send_byte(0x40);	//write data
	if(i2c_wait_ack(5000))	//等待应答
		return 1;
	
	oled_send_byte(byte);
	if(i2c_wait_ack(5000))	//等待应答
		return 1;
	
	i2c_stop();
	
	return 0;

}

/*
************************************************************
*	函数名称：	oled_write_cmd
*
*	函数功能：	OLED写入一个命令
*
*	入口参数：	cmd：需要写入的命令
*
*	返回参数：	写入结果
*
*	说明：		0-成功		1-失败
************************************************************
*/
_Bool oled_write_cmd(unsigned char cmd)
{
	
	i2c_start();
	
	oled_send_byte(OLED_ADDRESS);	//设备地址
	if(i2c_wait_ack(5000))			//等待应答
		return 1;
	
	oled_send_byte(0x00);
	if(i2c_wait_ack(5000))			//等待应答
		return 1;
	
	oled_send_byte(cmd);
	if(i2c_wait_ack(5000))			//等待应答
		return 1;
	
	i2c_stop();
	
	return 0;
	
}

/*
************************************************************
*	函数名称：	oled_init
*
*	函数功能：	OLED初始化
*
*	入口参数：	无
*
*	返回参数：	无
*
*	说明：		
************************************************************
*/
void oled_init(void)
{
#if 1
	oled_write_cmd(0xAE); //关闭显示
	oled_write_cmd(0x20); //Set Memory Addressing Mode	
	oled_write_cmd(0x10); //00,Horizontal Addressing Mode;01,Vertical Addressing Mode;10,Page Addressing Mode (RESET);11,Invalid
	oled_write_cmd(0xb0); //Set Page Start Address for Page Addressing Mode,0-7
	oled_write_cmd(0xa1); //0xa0，X轴正常显示；0xa1，X轴镜像显示
	oled_write_cmd(0xc8); //0xc0，Y轴正常显示；0xc8，Y轴镜像显示
	oled_write_cmd(0x00); //设置列地址低4位
	oled_write_cmd(0x10); //设置列地址高4位
	oled_write_cmd(0x40); //设置起始线地址
	oled_write_cmd(0x81); //设置对比度值
	oled_write_cmd(0x7f); //------
	oled_write_cmd(0xa6); //0xa6,正常显示模式;0xa7，
	oled_write_cmd(0xa8); //--set multiplex ratio(1 to 64)
	oled_write_cmd(0x3F); //------
	oled_write_cmd(0xa4); //0xa4,显示跟随RAM的改变而改变;0xa5,显示内容忽略RAM的内容
	oled_write_cmd(0xd3); //设置显示偏移
	oled_write_cmd(0x00); //------
	oled_write_cmd(0xd5); //设置内部显示时钟频率
	oled_write_cmd(0xf0); //------
	oled_write_cmd(0xd9); //--set pre-charge period//
	oled_write_cmd(0x22); //------
	oled_write_cmd(0xda); //--set com pins hardware configuration//
	oled_write_cmd(0x12); //------
	oled_write_cmd(0xdb); //--set vcomh//
	oled_write_cmd(0x20); //------
	oled_write_cmd(0x8d); //--set DC-DC enable//
	oled_write_cmd(0x14); //------
	oled_write_cmd(0xaf); //打开显示
#else
	oled_write_cmd(0xAE);   //display off
	oled_write_cmd(0x00);	//Set Memory Addressing Mode	
	oled_write_cmd(0x10);	//00,Horizontal Addressing Mode;01,Vertical Addressing Mode;10,Page Addressing Mode (RESET);11,Invalid
	oled_write_cmd(0x40);	//Set Page Start Address for Page Addressing Mode,0-7
	oled_write_cmd(0xb0);	//Set COM Output Scan Direction
	oled_write_cmd(0x81);//---set low column address
	oled_write_cmd(0xff);//---set high column address
	oled_write_cmd(0xa1);//--set start line address
	oled_write_cmd(0xa6);//--set contrast control register
	oled_write_cmd(0xa8);
	oled_write_cmd(0x3f);//--set segment re-map 0 to 127
	oled_write_cmd(0xad);//--set normal display
	oled_write_cmd(0x8b);//--set multiplex ratio(1 to 64)
	oled_write_cmd(0x33);//
	oled_write_cmd(0xc8);//0xa4,Output follows RAM content;0xa5,Output ignores RAM content
	oled_write_cmd(0xd3);//-set display offset
	oled_write_cmd(0x00);//-not offset
	oled_write_cmd(0xd5);//--set display clock divide ratio/oscillator frequency
	oled_write_cmd(0x80);//--set divide ratio
	oled_write_cmd(0xd9);//--set pre-charge period
	oled_write_cmd(0x1f); //
	oled_write_cmd(0xda);//--set com pins hardware configuration
	oled_write_cmd(0x12);
	oled_write_cmd(0xdb);//--set vcomh
	oled_write_cmd(0x40);//0x20,0.77xVcc
//	oled_write_cmd(0x8d);//--set DC-DC enable
//	oled_write_cmd(0x14);//
	oled_write_cmd(0xaf);//--turn on oled panel
#endif

}

/*
************************************************************
*	函数名称：	oled_set_address
*
*	函数功能：	设置OLED显示地址
*
*	入口参数：	x：行地址
*				y：列地址
*
*	返回参数：	无
*
*	说明：		
************************************************************
*/
void oled_set_address(unsigned char x, unsigned char y)
{

	oled_write_cmd(0xb0 + x);					//设置行地址
	HAL_Delay_us(iicInfo.speed);
	oled_write_cmd(((y & 0xf0) >> 4) | 0x10);	//设置列地址的高4位
	HAL_Delay_us(iicInfo.speed);
	oled_write_cmd(y & 0x0f);					//设置列地址的低4位
	HAL_Delay_us(iicInfo.speed);
	
}

/*
************************************************************
*	函数名称：	oled_clear_screen
*
*	函数功能：	OLED全屏清除
*
*	入口参数：	无
*
*	返回参数：	无
*
*	说明：		
************************************************************
*/
void oled_clear_screen(void)
{
	
	unsigned char i = 0, j = 0;
	
	i2c_speed_set(1);
		
	for(; i < 8; i++)
	{
		oled_write_cmd(0xb0 + i);
		oled_write_cmd(0x10);
		oled_write_cmd(0x00);
			
		for(j = 0; j < 132; j++)
		{
			oled_write_data(0x00);
		}
	}
	
}

/*
************************************************************
*	函数名称：	oled_clear_line
*
*	函数功能：	OLED清除指定行
*
*	入口参数：	x：需要清除的行
*
*	返回参数：	无
*
*	说明：		
************************************************************
*/
void oled_clear_line(unsigned char x)
{

	unsigned char i = 0;
		
	oled_write_cmd(0xb0 + x);
	oled_write_cmd(0x10);
	oled_write_cmd(0x00);
			
	for(; i < 132; i++)
	{
		oled_write_data(0x00);
	}

}

/*
************************************************************
*	函数名称：	oled_dis_128x64_picture
*
*	函数功能：	显示一幅128*64的图片
*
*	入口参数：	dp：图片数据指针
*
*	返回参数：	无
*
*	说明：		
************************************************************
*/
void oled_dis_128x64_picture(const unsigned char *dp)
{
	
	unsigned char i = 0, j = 0;
	
		
	for(; j < 8; j++)
	{
		oled_set_address(j, 0);
		
		for (i = 0; i < 128; i++)
		{	
			oled_write_data(*dp++); //写数据到LCD,每写完一个8位的数据后列地址自动加1
		}
	}
	
}

/*
************************************************************
*	函数名称：	oled_dis_16x16_char
*
*	函数功能：	显示16x16的点阵数据
*
*	入口参数：	dp：图片数据指针
*
*	返回参数：	无
*
*	说明：		显示16x16点阵图像、汉字、生僻字或16x16点阵的其他图标
************************************************************
*/
void oled_dis_16x16_char(unsigned short x, unsigned short y, const unsigned char *dp)
{
	
	unsigned short i = 0, j = 0;
	
	i2c_speed_set(1);					//i2c速度控制
		
	for(j = 2; j > 0; j--)
	{
		oled_set_address(x, y);
		
		for (i = 0; i < 16; i++)
		{
			oled_write_data(*dp++);		//写数据到OLED,每写完一个8位的数据后列地址自动加1
		}
		
		x++;
	}
	
}

/*
************************************************************
*	函数名称：	oled_dis_6x8_string
*
*	函数功能：	显示6x8的点阵数据
*
*	入口参数：	x：显示行
*				y：显示列
*				fmt：不定长参
*
*	返回参数：	无
*
*	说明：		能显示7行
************************************************************
*/
void oled_dis_6x8_string(unsigned char x, unsigned char y, char *fmt, ...)
{

	unsigned char i = 0, ch = 0;
	unsigned char OledPrintfBuf[300];
	
	va_list ap;
	unsigned char *pStr = OledPrintfBuf;
	
	va_start(ap,fmt);
	vsprintf((char *)OledPrintfBuf, fmt, ap);
	va_end(ap);
	
	y += 2;
	i2c_speed_set(1);							//i2c速度控制
		
	while(*pStr != '\0')
	{
		ch = *pStr - 32;
		
		if(y > 126)
		{
			y = 2;
			x++;
		}
		
		oled_set_address(x, y);
		for(i = 0; i < 6; i++)
			oled_write_data(F6x8[ch][i]);
		
		y += 6;
		pStr++;
	}

}

/*
************************************************************
*	函数名称：	oled_dis_8x16_string
*
*	函数功能：	显示8x16的点阵数据
*
*	入口参数：	x：显示行
*				y：显示列
*				fmt：不定长参
*
*	返回参数：	无
*
*	说明：		能显示4行
************************************************************
*/
void oled_dis_8x16_string(unsigned char x, unsigned char y, char *fmt, ...)
{

	unsigned char i = 0, ch = 0;
	unsigned char OledPrintfBuf[300];
	
	va_list ap;
	unsigned char *pStr = OledPrintfBuf;
	
	va_start(ap,fmt);
	vsprintf((char *)OledPrintfBuf, fmt, ap);
	va_end(ap);
	
	y += 2;
	i2c_speed_set(1);							//i2c速度控制
		
	while(*pStr != '\0')
	{
		ch = *pStr - 32;
			
		if(y > 128)
		{
			y = 2;
			x += 2;
		}
			
		oled_set_address(x, y);
		for(i = 0; i < 8; i++)
			oled_write_data(F8X16[(ch << 4) + i]);
		
		oled_set_address(x + 1, y);
		for(i = 0; i < 8; i++)
			oled_write_data(F8X16[(ch << 4) + i + 8]);
		
		y += 8;
		pStr++;
	}

}
