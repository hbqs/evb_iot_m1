/*
 * File      : main.c
 * This file is part of main.c in qsdk
 * Copyright (c) 2018-2030, longmain Development Team
 *
 * Change Logs:
 * Date           Author       Notes
 * 2018-11-14     longmain     first version
 */
#include "main.h"
#include "rtc.h"
#include "qsdk.h"
#include "gpio.h"
#include "beep.h"
#include "led.h"
#include "key.h"
#include "delay.h"
#include "i2c.h"
#include "oled.h"
#include "sht20.h"

//引入C库
#include <stdlib.h>
#include "stdio.h"
#include <string.h>

extern char led0_status[2];
extern char led1_status[2];
extern char led2_status[2];
extern char led3_status[2];
extern keysTypedef_t keys;
extern RTC_TimeTypeDef RTC_TimeStructure;	
extern RTC_DateTypeDef RTC_DateStructure;

//定义任务控制块
static rt_thread_t led_thread_id=RT_NULL;
static rt_thread_t net_thread_id=RT_NULL;
static rt_thread_t sht20_thread_id=RT_NULL;
static rt_thread_t rtc_thread_id=RT_NULL;
static rt_thread_t data_thread_id=RT_NULL;
static rt_thread_t key_thread_id=RT_NULL;											 
											 
											 
void led1_thread_entry(void* parameter);
void net_thread_entry(void* parameter);
void sht20_thread_entry(void* parameter);
void rtc_thread_entry(void* parameter);
void data_thread_entry(void* parameter);
void key_thread_entry(void* parameter);

int min_temp=10,max_temp=50;
int min_hump=10,max_hump=50;

char temp[10]="";
char hump[10]="";
char lat_value[20]="3700.71191";
char lon_value[20]="11517.78628";
char exec_str[50]="7104f10400000000f20100f30100f40100";

//使用IOT的情况下，上报数据用的数组
char update[100]="";

//使用NET的情况下，上报信息标志
char net_update_status=0;

#ifdef QSDK_USING_ONENET
/********************************************
***     DEVICE成员值
***
*** 1  objectid         	object id
*** 2  instancecount    	instance 数量
*** 3  instancebitmap   	instance bitmap
*** 4  attributecount   	attribute count (具有Read/Write操作的object有attribute)
*** 5  actioncount      	action count (具有Execute操作的object有action)
*** 6  instanceid       	instance id
*** 7  resourceid					resource id
*** 8  valuetype					数据类型
*** 9  len								数据长度
*** 10 flge								消息标志:默认为0
*** 11 msgid							消息ID:默认为0
*** 12 up_status					数据上报开关
*** 13 value							数据值
*********************************************/
#ifdef QSDK_USING_ME3616
DEVICE onenet_device[]={											 
											 {temp_objectid,1,"1",1,1,temp_instanceid,temp_resourceid,qsdk_onenet_value_Float,0,0,0,0,temp},
											 {temp_objectid,1,"1",1,1,temp_exec_instanceid,temp_exec_resourceid,qsdk_onenet_value_Opaque,0,0,0,0,"012345"},
											 {hump_objectid,1,"1",1,1,hump_instanceid,hump_resourceid,qsdk_onenet_value_Float,0,0,0,0,hump},
											 {hump_objectid,1,"1",1,1,hump_exec_instanceid,hump_exec_resourceid,qsdk_onenet_value_Opaque,0,0,0,0,"012345"},
											 {light0_objectid,3,"111",1,0,light0_instanceid,light0_resourceid,qsdk_onenet_value_Bool,0,0,0,0,led1_status},
											 {light1_objectid,3,"111",1,0,light1_instanceid,light1_resourceid,qsdk_onenet_value_Bool,0,0,0,0,led2_status},
											 {light2_objectid,3,"111",1,0,light2_instanceid,light2_resourceid,qsdk_onenet_value_Bool,0,0,0,0,led3_status},
											 {gps_objectid,1,"1",2,0,gps_instanceid,lat_resourceid,qsdk_onenet_value_String,0,0,0,0,lat_value},
											 {gps_objectid,1,"1",2,0,gps_instanceid,lon_resourceid,qsdk_onenet_value_String,0,0,0,0,lon_value},
											 };
	
#else 
DEVICE onenet_device[]={											 
											 {temp_objectid,2,"11",1,1,temp_instanceid,temp_resourceid,qsdk_onenet_value_Float,0,0,0,0,temp},
											 {temp_objectid,2,"11",1,1,temp_exec_instanceid,temp_exec_resourceid,qsdk_onenet_value_Opaque,0,0,0,0,"012345"},
											 {hump_objectid,2,"11",1,1,hump_instanceid,hump_resourceid,qsdk_onenet_value_Float,0,0,0,0,hump},
											 {hump_objectid,2,"11",1,1,hump_exec_instanceid,hump_exec_resourceid,qsdk_onenet_value_Opaque,0,0,0,0,"012345"},
											 {light0_objectid,3,"111",1,0,light0_instanceid,light0_resourceid,qsdk_onenet_value_Bool,0,0,0,0,led1_status},
											 {light1_objectid,3,"111",1,0,light1_instanceid,light1_resourceid,qsdk_onenet_value_Bool,0,0,0,0,led2_status},
											 {light2_objectid,3,"111",1,0,light2_instanceid,light2_resourceid,qsdk_onenet_value_Bool,0,0,0,0,led3_status},
											 
											 };
#endif

#endif
/**
  * @brief  The application entry point.
  *
  * @retval None
  */
int main(void)
{
	MX_RTC_Init();
	printf("欢迎使用骑士智能科技 EVB_IOT_M1 开发板\r\n");
	oled_clear_screen();
	oled_dis_16x16_char(0, 16, Qi);
	oled_dis_16x16_char(0, 32, Shi);
	oled_dis_16x16_char(0, 48, Zhi);
	oled_dis_16x16_char(0, 64, Neng);
	oled_dis_16x16_char(0, 80, Ke);
	oled_dis_16x16_char(0, 96, Ji);
	oled_dis_8x16_string(2,24,"EVB_IOT_M1");
	oled_dis_16x16_char(4, 0, wen);
	oled_dis_16x16_char(4, 16, du);
	oled_dis_8x16_string(4,32,":");

	oled_dis_16x16_char(6, 0, shi);
	oled_dis_16x16_char(6, 16, du);
	oled_dis_8x16_string(6,32,":");
	
	led_thread_id=rt_thread_create("led_thread",
																 led1_thread_entry,
																 RT_NULL,
																 500,
																 15,
																 20);
		if(led_thread_id!=RT_NULL)
			rt_thread_startup(led_thread_id);
		else
			return -1;
	net_thread_id=rt_thread_create("net_thread",
																 net_thread_entry,
																 RT_NULL,
																 800,
																 10,
																 20);
		if(net_thread_id!=RT_NULL)
			rt_thread_startup(net_thread_id);
		else
			return -1;
	sht20_thread_id=rt_thread_create("sht20_thread",
																 sht20_thread_entry,
																 RT_NULL,
																 800,
																 19,
																 20);
		if(sht20_thread_id!=RT_NULL)
			rt_thread_startup(sht20_thread_id);
		else
			return -1;
	rtc_thread_id=rt_thread_create("rtc_thread",
																 rtc_thread_entry,
																 RT_NULL,
																 500,
																 18,
																 20);
		if(rtc_thread_id!=RT_NULL)
			rt_thread_startup(rtc_thread_id);
		else
			return -1;
	data_thread_id=rt_thread_create("data_thread",
																 data_thread_entry,
																 RT_NULL,
																 500,
																 12,
																 50);
		if(data_thread_id==RT_NULL)
			return -1;
		
	key_thread_id=rt_thread_create("key_thread",
																 key_thread_entry,
																 RT_NULL,
																 500,
																 5,
																 20);
		if(key_thread_id!=RT_NULL)
			rt_thread_startup(key_thread_id);
		else
			return -1;
	
}

 at_response_t resp = RT_NULL;
int result = 0;

void led1_thread_entry(void* parameter)
{
	
	while(1)
	{
		if(nb_device.net_connect_ok==0)
		{
			qsdk_hw_led_on(0);
			rt_thread_delay(200);
			qsdk_hw_led_off(0);
			rt_thread_delay(800);
		}
		else
		{
			qsdk_hw_led_on(0);
			rt_thread_delay(100);
			qsdk_hw_led_off(0);
			rt_thread_delay(100);
		}
	}
}


void net_thread_entry(void* parameter)
{
	int i=0;int net_socket;
	if(qsdk_nb_hw_init()!=RT_EOK)
	{
		printf("module init failure\r\n");
		goto fail;
	}
#ifdef QSDK_USING_IOT

#ifdef QSDK_USING_ME3616
if(qsdk_iot_reg()==RT_EOK)
			printf("iot reg success\r\n");
		else
		{
			printf("iot reg failure\r\n");
			goto fail;
		}
		rt_thread_delay(1000);

#endif
	
#endif


#ifdef QSDK_USING_ONENET
	
//开启onenet后进入ONENET 一键初始化函数
	
#ifdef QSDK_USING_ME3616
		if(qsdk_onenet_init(onenet_device,sizeof(onenet_device)/sizeof(onenet_device[0]),3000)!=RT_EOK)	goto fail;
#endif
	
#if (defined QSDK_USING_M5310)||(defined QSDK_USING_M5310A)
		if(qsdk_onenet_init(onenet_device,sizeof(onenet_device)/sizeof(onenet_device[0]),3000)!=RT_EOK)	goto fail;
#endif
		
		qsdk_hw_beep_on();
		rt_thread_delay(100);
		qsdk_hw_beep_off();
#endif	

//如果开启了ME3616 GPS支持
#ifdef QSDK_USING_ME3616
#ifdef QSDK_USING_ME3616_GPS

	if(qsdk_agps_config()!=RT_EOK)
	{
		printf(" gps config failure\r\n");
		goto fail;
	}
	else printf("gps run success\r\n");
#endif
#endif
		rt_thread_startup(data_thread_id);
	while(1)
	{
#ifdef QSDK_USING_ONENET
		//维持设备在线时间
		if(qsdk_onenet_update_time(0)!=RT_EOK)
		{
			printf("qsdk update time failure\r\n");
			goto fail;
		}
		printf("qsdk update time success\r\n");
		
#endif

#ifdef QSDK_USING_NET		
			if(i==0)
			{		
				i=1;	
#if (defined QSDK_USING_M5310)||(defined QSDK_USING_M5310A)	
					// 创建网络套子号
				if(qsdk_net_create_socket(2,5688,"47.92.236.118",9013)!=RT_EOK)	
				{
					printf("\r\n create net socket failure");
					goto fail;
				}
				printf("\r\n create net socket success");
				net_update_status=1;
			}

#elif (defined QSDK_USING_ME3616)


				if(qsdk_net_create_socket(QSDK_NET_TYPE_UDP,&net_socket)!=RT_EOK)
				{
					printf("create net socket failure\r\n");
					goto fail;
				}
				printf("crete net socket success\r\n");
				printf("socket=%d\r\n",net_socket);
				if(qsdk_net_connect_to_server(net_socket,"47.92.236.118",9013)!=RT_EOK)
				{
					printf("connect server failure\r\n");
					goto fail;
				}
				printf("connect server success\r\n");
				if(qsdk_net_send_data(net_socket,"1234",2)!=RT_EOK)
				{
					printf("net send data failure\r\n");
				}
				printf("net send data success\r\n");
			}
			rt_thread_delay(30000);
#endif
#endif
#ifdef QSDK_USING_IOT

#if (defined QSDK_USING_M5310)||(defined QSDK_USING_M5310A)
		if(nb_device.net_connect_ok)
			rt_thread_delay(5000);
			if(qsdk_iot_send_date("1234567890")!=RT_EOK)
			{
				printf("iot updte failure\r\n");
				goto fail;
			}
			printf("iot updte success\r\n");
		rt_thread_delay(15000);
#endif

#ifdef QSDK_USING_ME3616

		
		printf("nb_init OK\r\n");
		if(nb_device.net_connect_ok)
		{
				if(qsdk_iot_update("1234567890,,,!!!...，，，。。。！！！")!=RT_EOK)
					printf("iot update failure\r\n");
			
		}	
		rt_thread_delay(30000);
#endif

#endif
			
			
		//判断当前网络是否在线，如果不在线则关闭任务
		if(qsdk_nb_get_net_start()!=RT_EOK)
		{
			printf("get net status failure\r\n");
			if(qsdk_nb_get_net_start()!=RT_EOK)
			{
				printf("get net status failure\r\n");
				
				//挂起数据上报任务
				rt_thread_suspend(data_thread_id);
				goto fail;
				
			}
		}
		//如果当前网络为断开状态
		if(nb_device.net_connect_ok==0)
		{
			printf("net connect failure\r\n");
			//挂起数据上报任务
			rt_thread_suspend(data_thread_id);
			goto fail;
		}
		printf("net connect success\r\n");

		rt_thread_delay(200000);
	}
	fail:
	rt_kprintf("net thread close success\r\n");
	rt_thread_delete(net_thread_id);
	rt_schedule();
}

void sht20_thread_entry(void* parameter)
{
	printf("SHT20 task is open\r\n");
	while(1)
	{
		sht20_get_value();
		sprintf(temp,"%0.2f",sht20Info.tempreture);
		sprintf(hump,"%0.2f",sht20Info.humidity);
		oled_dis_8x16_string(4,40,"%0.1f",sht20Info.tempreture);
		oled_dis_8x16_string(6,40,"%0.1f",sht20Info.humidity);
		rt_thread_delay(1000);
	}
}

void rtc_thread_entry(void* parameter)
{
	printf("RTC task is open\r\n");
	while(1)
	{
		if(nb_device.net_connect_ok)
		{
			rt_thread_delay(1000);
			rt_enter_critical();
#ifdef QSDK_USING_IOT		
			//把需要上传到平台的参数进行封装
			memset(update,0,sizeof(update));
			sprintf(update,"temp=%0.2f,hump=%0.2f",sht20Info.tempreture,sht20Info.humidity);
#endif
			HAL_RTC_GetTime(&hrtc,&RTC_TimeStructure,RTC_FORMAT_BIN);
			HAL_RTC_GetDate(&hrtc,&RTC_DateStructure,RTC_FORMAT_BIN);
		
		//	printf("\r\ndata:20%02d-%02d-%02d,time:%02d:%02d:%02d\r\n",RTC_DateStructure.Year,RTC_DateStructure.Month,RTC_DateStructure.Date,RTC_TimeStructure.Hours,RTC_TimeStructure.Minutes,RTC_TimeStructure.Seconds);
		//	printf("温度：%0.1f  湿度：%0.1f\r\n",sht20Info.tempreture,sht20Info.humidity);
			rt_exit_critical();
		}
		else 	rt_thread_delay(500);
	}
}

void data_thread_entry(void* parameter)
{

	printf("data task is open\r\n");
	rt_thread_delay(500);
	while(1)
	{
#ifdef QSDK_USING_ONENET
		//判断onenet时候连接成功，连接成功便上报所有数据
		if(qsdk_onenet_get_connect()==RT_EOK)
		{
			printf("data UP is open\r\n");	
			if(qsdk_onenet_notify_data_to_net(0)==RT_EOK)
					printf("notify data success\r\n");
			else	printf("notify data failure\r\n");			
		}
#endif
	
#ifdef QSDK_USING_IOT
	if(nb_device.net_connect_ok)
		{
			if(qsdk_iot_update(update)!=RT_EOK)
			{
				printf("iot updte failure\r\n");
				goto fail;
			}
			printf("iot updte success\r\n");
		}
#endif

#ifdef QSDK_USING_NET

			if(net_update_status)
			{
			//发送数据到服务器， 此处为字符串，程序中已经转换为HEX
				if(qsdk_net_send_data(2,"1234567890",10)!=RT_EOK)
				{
					printf("\r\n send net data failure");	
					goto fail;
				}
				printf("\r\n send net data success");
			}

#endif
		rt_thread_delay(60000);
	}
			fail:
	rt_kprintf("data thread close success\r\n");
	rt_thread_delete(data_thread_id);
	rt_schedule();
	
}

void key_thread_entry(void* parameter)
{

	printf("key task is open\r\n");
	while(1)
	{
		qsdk_hw_key_Handle((keysTypedef_t *)&keys);
		rt_thread_delay(10);
#ifdef QSDK_USING_ONENET

//判断是否有需要上报的数据，有的话上报数据
		if(data_stream.update_status)
		{
			if(qsdk_onenet_notify_data_to_net(1)==RT_EOK)
				printf("notify data success\r\n ");
			else	printf("notify data failure\r\n ");
		}		
#endif
	}
}


/**
  * @brief  This function is executed in case of error occurrence.
  * @param  file: The file name as string.
  * @param  line: The line in file as a number.
  * @retval None
  */
void _Error_Handler(char *file, int line)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  while(1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t* file, uint32_t line)
{ 
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/**
  * @}
  */

/**
  * @}
  */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
