#include "i2c.h"
#include "usart.h"
#include "stdio.h"
#include "delay.h"

IIC_INFO iicInfo;


/************************************************************
*	函数名称：	i2c_speed_set
*
*	函数功能：	软件IIC速度控制
*
*	入口参数：	speed：延时参数
*
*	返回参数：	无
*
*	说明：		单位：微秒
************************************************************/
void i2c_speed_set(unsigned short speed)
{

	iicInfo.speed = speed;

}

/*
************************************************************
*	函数名称：	i2c_init
*
*	函数功能：	软件IIC总线IO初始化
*
*	入口参数：	无
*
*	返回参数：	无
*
*	说明：		使用开漏方式，这样可以不用切换IO口的输入输出方向
************************************************************
*/
void rt_hw_i2c_init(void)
{
	i2c_speed_set(5);
	
	SDA_H;													//拉高SDA线，处于空闲状态
	SCL_H;													//拉高SCL线，处于空闲状态

}

/*
************************************************************
*	函数名称：	i2c_start
*
*	函数功能：	软件IIC开始信号
*
*	入口参数：	无
*
*	返回参数：	无
*
*	说明：		
************************************************************
*/
void i2c_start(void)
{
	
	SDA_H;						//拉高SDA线
	SCL_H;						//拉高SCL线
	HAL_Delay_us(iicInfo.speed);		//延时，速度控制
	
	SDA_L;						//当SCL线为高时，SDA线一个下降沿代表开始信号
	HAL_Delay_us(iicInfo.speed);		//延时，速度控制
	SCL_L;						//钳住SCL线，以便发送数据

}

/*
************************************************************
*	函数名称：	i2c_stop
*
*	函数功能：	软件IIC停止信号
*
*	入口参数：	无
*
*	返回参数：	无
*
*	说明：		
************************************************************
*/
void i2c_stop(void)
{

	SDA_L;						//拉低SDA线
	SCL_L;						//拉低SCL先
	HAL_Delay_us(iicInfo.speed);		//延时，速度控制
	
	SCL_H;						//拉高SCL线
	SDA_H;						//拉高SDA线，当SCL线为高时，SDA线一个上升沿代表停止信号
	HAL_Delay_us(iicInfo.speed);

}

/*
************************************************************
*	函数名称：	i2c_wait_ack
*
*	函数功能：	软件IIC等待应答
*
*	入口参数：	timeOut：超时时间
*
*	返回参数：	无
*
*	说明：		单位：微秒
************************************************************
*/
_Bool i2c_wait_ack(unsigned int timeOut)
{
	
	
	SDA_H;
	HAL_Delay_us(iicInfo.speed);			//拉高SDA线
	
	SCL_H;
	HAL_Delay_us(iicInfo.speed);			//拉高SCL线
	
	while(SDA_R)							//如果读到SDA线为1，则等待。应答信号应是0
	{
		if(--timeOut == 0)
		{
			printf( "WaitAck TimeOut\r\n");

			i2c_stop();						//超时未收到应答，则停止总线
			
			return IIC_Err;					//返回失败
		}
		
		HAL_Delay_us(iicInfo.speed);
	}
	
	SCL_L;									//拉低SCL线，以便继续收发数据
	
	return IIC_OK;							//返回成功
	
}

/*
************************************************************
*	函数名称：	i2c_ack
*
*	函数功能：	软件IIC产生一个应答
*
*	入口参数：	无
*
*	返回参数：	无
*
*	说明：		当SDA线为低时，SCL线一个正脉冲代表发送一个应答信号
************************************************************
*/
void i2c_ack(void)
{
	
	SCL_L;						//拉低SCL线
	SDA_L;						//拉低SDA线
	HAL_Delay_us(iicInfo.speed);
	SCL_H;						//拉高SCL线
	HAL_Delay_us(iicInfo.speed);
	SCL_L;						//拉低SCL线
	
}

/*
************************************************************
*	函数名称：	i2c_no_ack
*
*	函数功能：	软件IIC产生一非个应答
*
*	入口参数：	无
*
*	返回参数：	无
*
*	说明：		当SDA线为高时，SCL线一个正脉冲代表发送一个非应答信号
************************************************************
*/
void i2c_no_ack(void)
{
	
	SCL_L;						//拉低SCL线
	SDA_H;						//拉高SDA线
	HAL_Delay_us(iicInfo.speed);
	SCL_H;						//拉高SCL线
	HAL_Delay_us(iicInfo.speed);
	SCL_L;						//拉低SCL线
	
}

/*
************************************************************
*	函数名称：	i2c_send_byte
*
*	函数功能：	软件IIC发送一个字节
*
*	入口参数：	byte：需要发送的字节
*
*	返回参数：	无
*
*	说明：		
************************************************************
*/
void i2c_send_byte(unsigned char byte)
{

	unsigned char count = 0;
	
    SCL_L;							//拉低时钟开始数据传输
	
    for(; count < 8; count++)		//循环8次，每次发送一个bit
    {
		if(byte & 0x80)				//发送最高位
			SDA_H;
		else
			SDA_L;
		
		byte <<= 1;					//byte左移1位
		
		HAL_Delay_us(iicInfo.speed);
		SCL_H;
		HAL_Delay_us(iicInfo.speed);
		SCL_L;
    }

}

/*
************************************************************
*	函数名称：	i2c_recv_byte
*
*	函数功能：	软件IIC接收一个字节
*
*	入口参数：	无
*
*	返回参数：	接收到的字节数据
*
*	说明：		
************************************************************
*/
unsigned char i2c_recv_byte(void)
{
	
	unsigned char count = 0, receive = 0;
	
	SDA_H;							//拉高SDA线，开漏状态下，需线拉高以便读取数据
	
    for(; count < 8; count++ )		//循环8次，每次发送一个bit
	{
		SCL_L;
		HAL_Delay_us(iicInfo.speed);
		SCL_H;
		
        receive <<= 1;				//左移一位
		
        if(SDA_R)					//如果SDA线为1，则receive变量自增，每次自增都是对bit0的+1，然后下一次循环会先左移一次
			receive++;
		
		HAL_Delay_us(iicInfo.speed);
    }
	
    return receive;
	
}

/*
************************************************************
*	函数名称：	i2c_write_byte
*
*	函数功能：	软件IIC写一个数据
*
*	入口参数：	slaveAddr：从机地址
*				regAddr：寄存器地址
*				byte：需要写入的数据
*
*	返回参数：	0-写入成功	1-写入失败
*
*	说明：		*byte是缓存写入数据的变量的地址，因为有些寄存器只需要控制下寄存器，并不需要写入值
************************************************************
*/
_Bool i2c_write_byte(unsigned char slaveAddr, unsigned char regAddr, unsigned char *byte)
{

	unsigned char addr = 0;

	addr = slaveAddr << 1;		//IIC地址是7bit，这里需要左移1位，bit0：1-读	0-写
	
	i2c_start();				//起始信号
	
	i2c_send_byte(addr);			//发送设备地址(写)
	if(i2c_wait_ack(5000))		//等待应答
		return IIC_Err;
	
	i2c_send_byte(regAddr);		//发送寄存器地址
	if(i2c_wait_ack(5000))		//等待应答
		return IIC_Err;
	
	if(byte)
	{
		i2c_send_byte(*byte);	//发送数据
		if(i2c_wait_ack(5000))	//等待应答
			return IIC_Err;
	}
	
	i2c_stop();					//停止信号
	
	return IIC_OK;

}

/*
************************************************************
*	函数名称：	i2c_read_byte
*
*	函数功能：	软件IIC读取一个字节
*
*	入口参数：	slaveAddr：从机地址
*				regAddr：寄存器地址
*				val：需要读取的数据缓存
*
*	返回参数：	0-成功		1-失败
*
*	说明：		val是一个缓存变量的地址
************************************************************
*/
_Bool i2c_read_byte(unsigned char slaveAddr, unsigned char regAddr, unsigned char *val)
{

	unsigned char addr = 0;

    addr = slaveAddr << 1;		//IIC地址是7bit，这里需要左移1位，bit0：1-读	0-写
	
	i2c_start();				//起始信号
	
	i2c_send_byte(addr);			//发送设备地址(写)
	if(i2c_wait_ack(5000))		//等待应答
		return IIC_Err;
	
	i2c_send_byte(regAddr);		//发送寄存器地址
	if(i2c_wait_ack(5000))		//等待应答
		return IIC_Err;
	
	i2c_start();				//重启信号
	
	i2c_send_byte(addr + 1);		//发送设备地址(读)
	if(i2c_wait_ack(5000))		//等待应答
		return IIC_Err;
	
	*val = i2c_recv_byte();		//接收
	i2c_no_ack();					//产生一个非应答信号，代表读取接收
	
	i2c_stop();					//停止信号
	
	return IIC_OK;

}

/*
************************************************************
*	函数名称：	i2c_write_bytes
*
*	函数功能：	软件IIC写多个数据
*
*	入口参数：	slaveAddr：从机地址
*				regAddr：寄存器地址
*				buf：需要写入的数据缓存
*				num：数据长度
*
*	返回参数：	0-写入成功	1-写入失败
*
*	说明：		*buf是一个数组或指向一个缓存区的指针
************************************************************
*/
_Bool i2c_write_bytes(unsigned char slaveAddr, unsigned char regAddr, unsigned char *buf, unsigned char num)
{

	unsigned char addr = 0;

	addr = slaveAddr << 1;		//IIC地址是7bit，这里需要左移1位，bit0：1-读	0-写
	
	i2c_start();				//起始信号
	
	i2c_send_byte(addr);			//发送设备地址(写)
	if(i2c_wait_ack(5000))		//等待应答
		return IIC_Err;
	
	i2c_send_byte(regAddr);		//发送寄存器地址
	if(i2c_wait_ack(5000))		//等待应答
		return IIC_Err;
	
	while(num--)				//循环写入数据
	{
		i2c_send_byte(*buf);		//发送数据
		if(i2c_wait_ack(5000))	//等待应答
			return IIC_Err;
		
		buf++;					//数据指针偏移到下一个
		
		HAL_Delay_us(10);
	}
	
	i2c_stop();					//停止信号
	
	return IIC_OK;

}

/*
************************************************************
*	函数名称：	i2c_read_bytes
*
*	函数功能：	软件IIC读多个数据
*
*	入口参数：	slaveAddr：从机地址
*				regAddr：寄存器地址
*				buf：需要读取的数据缓存
*				num：数据长度
*
*	返回参数：	0-写入成功	1-写入失败
*
*	说明：		*buf是一个数组或指向一个缓存区的指针
************************************************************
*/
_Bool i2c_read_bytes(unsigned char slaveAddr, unsigned char regAddr, unsigned char *buf, unsigned char num)
{

	unsigned short addr = 0;

    addr = slaveAddr << 1;		//IIC地址是7bit，这里需要左移1位，bit0：1-读	0-写
	
	i2c_start();				//起始信号
	
	i2c_send_byte(addr);			//发送设备地址(写)
	if(i2c_wait_ack(5000))		//等待应答
		return IIC_Err;
	
	i2c_send_byte(regAddr);		//发送寄存器地址
	if(i2c_wait_ack(5000))		//等待应答
		return IIC_Err;
	
	i2c_start();				//重启信号
	
	i2c_send_byte(addr + 1);		//发送设备地址(读)
	if(i2c_wait_ack(5000))		//等待应答
		return IIC_Err;
	
	while(num--)
	{
		*buf = i2c_recv_byte();
		buf++;					//偏移到下一个数据存储地址
		
		if(num == 0)
        {
           i2c_no_ack();			//最后一个数据需要回NOACK
        }
        else
        {
          i2c_ack();			//回应ACK
		}
	}
	
	i2c_stop();
	
	return IIC_OK;

}
