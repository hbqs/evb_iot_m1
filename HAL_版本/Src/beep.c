#include "beep.h"
#include "main.h"
/**************************************
*函数名称：	qsdk_hw_beep_on
*
*作用：	打开蜂鸣器
*
*形参：	无
*
*返回值：	无
***************************************/
void qsdk_hw_beep_on(void)
{
	HAL_GPIO_WritePin(BEEP_GPIO_Port,BEEP_Pin,GPIO_PIN_SET);
}
/**************************************
*函数名称：	qsdk_hw_beep_off
*
*作用：	关闭蜂鸣器
*
*形参：	无
*
*返回值：	无
***************************************/
void qsdk_hw_beep_off(void)
{
	HAL_GPIO_WritePin(BEEP_GPIO_Port,BEEP_Pin,GPIO_PIN_RESET);
}
