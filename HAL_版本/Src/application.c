#include "board.h" 
#include "stm32l1xx.h"
#include "rtthread.h"
#include <stdlib.h>
#include "stdio.h"
#include <string.h>
#include "qsdk.h"
#include <at.h>
#include "key.h"

extern char led0_status[2];
extern char led1_status[2];
extern char led2_status[2];
extern char led3_status[2];
extern keysTypedef_t keys;  

//定义任务控制块
static rt_thread_t led_thread_id=RT_NULL;
static rt_thread_t net_thread_id=RT_NULL;
static rt_thread_t sht20_thread_id=RT_NULL;
static rt_thread_t rtc_thread_id=RT_NULL;
static rt_thread_t data_thread_id=RT_NULL;
static rt_thread_t key_thread_id=RT_NULL;											 
											 
											 
void led1_thread_entry(void* parameter);
void net_thread_entry(void* parameter);
void sht20_thread_entry(void* parameter);
void rtc_thread_entry(void* parameter);
void data_thread_entry(void* parameter);
void key_thread_entry(void* parameter);

int min_temp=10,max_temp=50;
int min_hump=10,max_hump=50;

char temp[10]="";
char hump[10]="";
char exec_str[50]="7104f10400000000f20100f30100f40100";

#ifdef QSDK_USING_ONENET
/********************************************
***     DEVICE成员值
***
*** 1  objectid         	object id
*** 2  instancecount    	instance 数量
*** 3  instancebitmap   	instance bitmap
*** 4  attributecount   	attribute count (具有Read/Write操作的object有attribute)
*** 5  actioncount      	action count (具有Execute操作的object有action)
*** 6  instanceid       	instance id
*** 7  resourceid					resource id
*** 8  valuetype					数据类型
*** 9  len								数据长度
*** 10 flge								消息标志:默认为0
*** 11 msgid							消息ID:默认为0
*** 12 up_status					数据上报开关
*** 13 value							数据值
*********************************************/
#ifdef QSDK_USING_ME3616
DEVICE onenet_device[]={											 
											 {temp_objectid,1,"1",1,1,temp_instanceid,temp_resourceid,qsdk_onenet_value_Float,0,0,0,0,temp},
											 {temp_objectid,1,"1",1,1,temp_exec_instanceid,temp_exec_resourceid,qsdk_onenet_value_Opaque,0,0,0,0,"012345"},
											 {hump_objectid,1,"1",1,1,hump_instanceid,hump_resourceid,qsdk_onenet_value_Float,0,0,0,0,hump},
											 {hump_objectid,1,"1",1,1,hump_exec_instanceid,hump_exec_resourceid,qsdk_onenet_value_Opaque,0,0,0,0,"012345"},
											 {light0_objectid,3,"111",1,0,light0_instanceid,light0_resourceid,qsdk_onenet_value_Bool,0,0,0,0,led1_status},
											 {light1_objectid,3,"111",1,0,light1_instanceid,light1_resourceid,qsdk_onenet_value_Bool,0,0,0,0,led2_status},
											 {light2_objectid,3,"111",1,0,light2_instanceid,light2_resourceid,qsdk_onenet_value_Bool,0,0,0,0,led3_status},
											 
											 };
	
#else 
DEVICE onenet_device[]={											 
											 {temp_objectid,2,"11",1,1,temp_instanceid,temp_resourceid,qsdk_onenet_value_Float,0,0,0,0,temp},
											 {temp_objectid,2,"11",1,1,temp_exec_instanceid,temp_exec_resourceid,qsdk_onenet_value_Opaque,0,0,0,0,"012345"},
											 {hump_objectid,2,"11",1,1,hump_instanceid,hump_resourceid,qsdk_onenet_value_Float,0,0,0,0,hump},
											 {hump_objectid,2,"11",1,1,hump_exec_instanceid,hump_exec_resourceid,qsdk_onenet_value_Opaque,0,0,0,0,"012345"},
											 {light0_objectid,3,"111",1,0,light0_instanceid,light0_resourceid,qsdk_onenet_value_Bool,0,0,0,0,led1_status},
											 {light1_objectid,3,"111",1,0,light1_instanceid,light1_resourceid,qsdk_onenet_value_Bool,0,0,0,0,led2_status},
											 {light2_objectid,3,"111",1,0,light2_instanceid,light2_resourceid,qsdk_onenet_value_Bool,0,0,0,0,led3_status},
											 
											 };
#endif

#endif
void qsdk_main_entry(void* parameter) 
{ 

	
	led_thread_id=rt_thread_create("led_thread",
																 led1_thread_entry,
																 RT_NULL,
																 500,
																 22,
																 20);
		if(led_thread_id!=RT_NULL)
			rt_thread_startup(led_thread_id);
		else
			return -1;
	net_thread_id=rt_thread_create("net_thread",
																 net_thread_entry,
																 RT_NULL,
																 800,
																 10,
																 20);
		if(net_thread_id!=RT_NULL)
			rt_thread_startup(net_thread_id);
		else
			return -1;
	sht20_thread_id=rt_thread_create("sht20_thread",
																 sht20_thread_entry,
																 RT_NULL,
																 800,
																 19,
																 20);
		if(sht20_thread_id!=RT_NULL)
			rt_thread_startup(sht20_thread_id);
		else
			return -1;
	rtc_thread_id=rt_thread_create("rtc_thread",
																 rtc_thread_entry,
																 RT_NULL,
																 500,
																 18,
																 20);
		if(rtc_thread_id!=RT_NULL)
			rt_thread_startup(rtc_thread_id);
		else
			return -1;
	data_thread_id=rt_thread_create("data_thread",
																 data_thread_entry,
																 RT_NULL,
																 500,
																 12,
																 50);
		if(data_thread_id!=RT_NULL)
			rt_thread_startup(data_thread_id);
		else
			return -1;
		
	key_thread_id=rt_thread_create("key_thread",
																 key_thread_entry,
																 RT_NULL,
																 500,
																 5,
																 20);
		if(key_thread_id!=RT_NULL)
			rt_thread_startup(key_thread_id);
		else
			return -1;
	
} 

   at_response_t resp = RT_NULL;
  int result = 0;

void led1_thread_entry(void* parameter)
{
	
	while(1)
	{
		if(nb_device.net_connect_ok==0)
		{
			rt_hw_led_on(0);
			rt_thread_delay(200);
			rt_hw_led_off(0);
			rt_thread_delay(800);
		}
		else
		{
			rt_hw_led_on(0);
			rt_thread_delay(100);
			rt_hw_led_off(0);
			rt_thread_delay(100);
		}
	}
}


void net_thread_entry(void* parameter)
{
	int i=0;int net_socket;
	if(qsdk_nb_hw_init()!=RT_EOK)
	{
		printf("module init failure\r\n");
		goto fail;
	}
#ifdef QSDK_USING_IOT

#ifdef QSDK_USING_ME3616
if(qsdk_iot_reg()==RT_EOK)
			printf("iot reg success\r\n");
		else
		{
			printf("iot reg failure\r\n");
			goto fail;
		}
		rt_thread_delay(1000);

#endif
	
#endif


#ifdef QSDK_USING_ONENET

#ifdef QSDK_USING_ME3616
		if(qsdk_onenet_init(onenet_device,sizeof(onenet_device)/sizeof(onenet_device[0]),3000)!=RT_EOK)	
#endif
	
#ifdef QSDK_USING_M5310
		if(qsdk_onenet_init(onenet_device,sizeof(onenet_device)/sizeof(onenet_device[0]),3000)!=RT_EOK)	
#endif
		{
				rt_kprintf(" error=%d onenet init failure \r\n",data_stream.error);
				goto fail;
			}
			rt_hw_beep_on();
			rt_thread_delay(100);
			rt_hw_beep_off();
		
#endif	
#ifdef QSDK_USING_ME3616_GPS
	if(qsdk_agps_config()!=RT_EOK)
	{
		printf(" gps config failure\r\n");
		goto fail;
	}
	else printf("gps run success\r\n");
#endif
	while(1)
	{
#ifdef QSDK_USING_ONENET
//		printf("notify data to onenet\r\n");
//		if(qsdk_notify_data_to_onenet(0)!=RT_EOK)
//			printf("notify data failure");
//		else	printf("notify data to onenet success");
		rt_thread_delay(100000);
#endif

#ifdef QSDK_USING_NET		
			if(i==0)
			{		
				i=1;	
#ifdef QSDK_USING_M5310			
				if(qsdk_net_create_socket(2,5688,"47.92.236.118",9013)!=RT_EOK)	
				{
					printf("\r\n create net socket failure");
					while(1);
				}
				printf("\r\n create net socket success");
				if(qsdk_net_send_data(2,"1234567890",10)!=RT_EOK)
				{
					printf("\r\n send net data failure");	
					while(1);
				}printf("\r\n send net data success");
			}
				rt_thread_delay(30000);
#elif (defined QSDK_USING_ME3616)


				if(qsdk_net_create_socket(QSDK_NET_TYPE_UDP,&net_socket)!=RT_EOK)
				{
					printf("create net socket failure\r\n");
					goto fail;
				}
				printf("crete net socket success\r\n");
				printf("socket=%d\r\n",net_socket);
				if(qsdk_net_connect_to_server(net_socket,"47.92.236.118",9013)!=RT_EOK)
				{
					printf("connect server failure\r\n");
					goto fail;
				}
				printf("connect server success\r\n");
				if(qsdk_net_send_data(net_socket,"1234",2)!=RT_EOK)
				{
					printf("net send data failure\r\n");
				}
				printf("net send data success\r\n");
			}
			rt_thread_delay(30000);
#endif
#endif
#ifdef QSDK_USING_IOT

#ifdef QSDK_USING_M5310
		if(nb_device.net_connect_ok)
			rt_thread_delay(5000);
			if(qsdk_iot_send_date("1234567890")!=RT_EOK)
			{
				printf("iot updte failure\r\n");
				goto fail;
			}
			printf("iot updte success\r\n");
		rt_thread_delay(15000);
#endif

#ifdef QSDK_USING_ME3616

		
		printf("nb_init OK\r\n");
		if(nb_device.net_connect_ok)
		{
				if(qsdk_iot_update("1234567890,,,!!!...，，，。。。！！！")!=RT_EOK)
					printf("iot update failure\r\n");
			
		}	
		rt_thread_delay(30000);
#endif

#endif
	}
	fail:
	rt_kprintf("net thread close success\r\n");
	rt_thread_delete(net_thread_id);
	rt_schedule();
}

void sht20_thread_entry(void* parameter)
{
	printf("SHT20 task is open\r\n");
	while(1)
	{
		sht20_get_value();
		rt_enter_critical();
		sprintf(temp,"%0.2f",sht20Info.tempreture);
		sprintf(hump,"%0.2f",sht20Info.humidity);
		oled_dis_8x16_string(4,40,"%0.1f",sht20Info.tempreture);
		oled_dis_8x16_string(6,40,"%0.1f",sht20Info.humidity);
		rt_exit_critical();
		rt_thread_delay(1000);
	}
}

void rtc_thread_entry(void* parameter)
{
	RTC_TimeTypeDef RTC_TimeStructure;	
	RTC_DateTypeDef RTC_DateStructure;
	printf("RTC task is open\r\n");
	while(1)
	{
		if(nb_device.net_connect_ok)
		{
			rt_thread_delay(1000);
			rt_enter_critical();
			RTC_GetTime(RTC_Format_BIN,&RTC_TimeStructure);
			RTC_GetDate(RTC_Format_BIN,&RTC_DateStructure);
		
		//	printf("\r\ndata:20%02d-%02d-%02d,time:%02d:%02d:%02d\r\n",RTC_DateStructure.RTC_Year,RTC_DateStructure.RTC_Month,RTC_DateStructure.RTC_Date,RTC_TimeStructure.RTC_Hours,RTC_TimeStructure.RTC_Minutes,RTC_TimeStructure.RTC_Seconds);
		//	printf("温度：%0.1f  湿度：%0.1f\r\n",sht20Info.tempreture,sht20Info.humidity);
			rt_exit_critical();
		}
		else 	rt_thread_delay(500);
	}
}

void data_thread_entry(void* parameter)
{

	printf("data task is open\r\n");
	while(1)
	{
#ifdef QSDK_USING_ONENET
		if(data_stream.connect_status==qsdk_onenet_status_success)
		{
			rt_thread_delay(60000);
			printf("data UP is open\r\n");	
			if(qsdk_notify_data_to_onenet(0)==RT_EOK)
					printf("notify data success\r\n ");
			else	printf("notify data failure\r\n ");
			
		}
		else 
#endif
		rt_thread_delay(1000);
	}
}

void key_thread_entry(void* parameter)
{

	printf("key task is open\r\n");
	while(1)
	{
		keyHandle((keysTypedef_t *)&keys);
		rt_thread_delay(10);
#ifdef QSDK_USING_ONENET

		if(data_stream.update_status)
		{
			if(qsdk_notify_data_to_onenet(1)==RT_EOK)
				printf("notify ack data success\r\n ");
			else	printf("notify ack data failure\r\n ");
		}

#endif
	}
}


