#include "stm32l1xx.h"

//硬件驱动函数
#include "sht20.h"
#include "main.h"
#include "i2c.h"
#include "delay.h"


const int16_t POLYNOMIAL = 0x131;

SHT20_INFO sht20Info;


/*
************************************************************
*	函数名称：	sht20_reset
*
*	函数功能：	SHT20复位
*
*	入口参数：	无
*
*	返回参数：	无
*
*	说明：		
************************************************************
*/
void sht20_reset(void)
{
	
    i2c_write_byte(SHT20_ADDRESS, SHT20_SOFT_RESET, (void *)0);
	
}

/*
************************************************************
*	函数名称：	sht20_read_user_reg
*
*	函数功能：	SHT20读取用户寄存器
*
*	入口参数：	无
*
*	返回参数：	读取到的用户寄存器的值
*
*	说明：		
************************************************************
*/
unsigned char  sht20_read_user_reg(void)
{
	
    unsigned char val = 0;
	
    i2c_read_byte(SHT20_ADDRESS, SHT20_READ_REG, &val);
	
    return val;
	
}

/*
************************************************************
*	函数名称：	sht20_check_crc
*
*	函数功能：	检查数据正确性
*
*	入口参数：	data：读取到的数据
*				nbrOfBytes：需要校验的数量
*				checksum：读取到的校对比验值
*
*	返回参数：	校验结果
*
*	说明：		0-成功		1-失败
************************************************************
*/
char sht20_check_crc(char data[], char nbrOfBytes, char checksum)
{
	
    char crc = 0;
    char bit = 0;
    char byteCtr = 0;
	
    //calculates 8-Bit checksum with given polynomial
    for(byteCtr = 0; byteCtr < nbrOfBytes; ++byteCtr)
    {
        crc ^= (data[byteCtr]);
        for ( bit = 8; bit > 0; --bit)
        {
            if (crc & 0x80) crc = (crc << 1) ^ POLYNOMIAL;
            else crc = (crc << 1);
        }
    }
	
    if(crc != checksum)
		return 1;
    else
		return 0;
	
}

/*
************************************************************
*	函数名称：	sht20_calc_Temp_value
*
*	函数功能：	温度计算
*
*	入口参数：	u16sT：读取到的温度原始数据
*
*	返回参数：	计算后的温度数据
*
*	说明：		
************************************************************
*/
float sht20_calc_Temp_value(unsigned short u16sT)
{
	
    float temperatureC = 0;            // variable for result

    u16sT &= ~0x0003;           // clear bits [1..0] (status bits)
    //-- calculate temperature [癈] --
    temperatureC = -46.85 + 175.72 / 65536 * (float)u16sT; //T= -46.85 + 175.72 * ST/2^16
	
    return temperatureC;
	
}

/*
************************************************************
*	函数名称：	SHT2x_calc_hump_value
*
*	函数功能：	湿度计算
*
*	入口参数：	u16sRH：读取到的湿度原始数据
*
*	返回参数：	计算后的湿度数据
*
*	说明：		
************************************************************
*/
float SHT2x_calc_hump_value(unsigned short u16sRH)
{
	
    float humidityRH = 0;              // variable for result
	
    u16sRH &= ~0x0003;          // clear bits [1..0] (status bits)
    //-- calculate relative humidity [%RH] --
    //humidityRH = -6.0 + 125.0/65536 * (float)u16sRH; // RH= -6 + 125 * SRH/2^16
    humidityRH = ((float)u16sRH * 0.00190735) - 6;
	
    return humidityRH;
	
}

/*
************************************************************
*	函数名称：	sht20_measure_hm
*
*	函数功能：	测量温湿度
*
*	入口参数：	cmd：测量温度还是湿度
*				pMeasurand：不为空则保存为ushort值到此地址
*
*	返回参数：	测量结果
*
*	说明：		
************************************************************
*/
float sht20_measure_hm(unsigned char cmd, unsigned short *pMeasurand)
{
	
    char  checksum = 0;  //checksum
    char  data[2];    //data array for checksum verification
	unsigned char addr = 0;
    unsigned short tmp = 0;
    float t = 0;
	
    addr = SHT20_ADDRESS << 1;
	
	i2c_start();
	
	i2c_send_byte(addr);
	if(i2c_wait_ack(50000)) //等待应答
		return 0.0;
	
	i2c_send_byte(cmd);
	if(i2c_wait_ack(50000)) //等待应答
		return 0.0;
	
	i2c_start();
	
	i2c_send_byte(addr + 1);
	while(i2c_wait_ack(50000)) //等待应答
	{
		i2c_start();
		i2c_send_byte(addr + 1);
	}
	
	HAL_Delay(70);
	
	data[0] = i2c_recv_byte();
	i2c_ack();
	data[1] = i2c_recv_byte();
	i2c_ack();
	
	checksum = i2c_recv_byte();
	i2c_ack();
	
	i2c_stop();
	
	sht20_check_crc(data, 2, checksum);
    tmp = (data[0] << 8) + data[1];
    if(cmd == SHT20_Measurement_T_HM)
    {
        t = sht20_calc_Temp_value(tmp);
    }
    else
    {
        t = SHT2x_calc_hump_value(tmp);
    }
	
    if(pMeasurand)
    {
        *pMeasurand = (unsigned short)t;
    }
	
    return t;
	
}

/*
************************************************************
*	函数名称：	sht20_get_value
*
*	函数功能：	获取温湿度数据
*
*	入口参数：	无
*
*	返回参数：	无
*
*	说明：		温湿度结果保存在SHT20结构体里
************************************************************
*/
void sht20_get_value(void)
{
	
	unsigned char val = 0;
	
	i2c_speed_set(5);
	
	sht20_read_user_reg();
	HAL_Delay_us(100);
	
	sht20Info.tempreture = sht20_measure_hm(SHT20_Measurement_T_HM, (void *)0);
	HAL_Delay(70);
	
	sht20Info.humidity = sht20_measure_hm(SHT20_Measurement_RH_HM, (void *)0);
	HAL_Delay(25);
	
	sht20_read_user_reg();
	HAL_Delay(25);
	
	i2c_write_byte(SHT20_ADDRESS, SHT20_WRITE_REG, &val);
	HAL_Delay_us(100);
	
	sht20_read_user_reg();
	HAL_Delay_us(100);
	
	sht20_reset();
	HAL_Delay_us(100);

}
