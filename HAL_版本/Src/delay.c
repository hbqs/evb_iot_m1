#include "delay.h"
#include "main.h"


/************************************************************
*	函数名称：	HAL_Delay_us
*
*	函数功能：	us 延时
*
*	入口参数：	time :  需要延时的时间
*
*	返回参数：	无
*
*	说明：		
************************************************************/
void HAL_Delay_us(int nus)
{
	int ticks;
	int told,tnow,tcnt=0;
	int reload=SysTick->LOAD;				//LOAD的值	    	 
	ticks=nus*4; 						//需要的节拍数 
	
	told=SysTick->VAL;        				//刚进入时的计数器值
	while(1)
	{
		tnow=SysTick->VAL;	
		if(tnow!=told)
		{	    
			if(tnow<told)tcnt+=told-tnow;	//这里注意一下SYSTICK是一个递减的计数器就可以了.
			else tcnt+=reload-tnow+told;	    
			told=tnow;
			if(tcnt>=ticks)break;			//时间超过/等于要延迟的时间,则退出.
		}  
	};
}
/**
 * This function will delay for some us.
 *
 * @param us the delay time of us
 */
void rt_hw_us_delay(rt_uint32_t us)
{
    rt_uint32_t delta;
    us = us * (SysTick->LOAD / (1000000 / RT_TICK_PER_SECOND));
    delta = SysTick->VAL;
    if (delta < us)
    {
        /* wait current OSTick left time gone */
        while (SysTick->VAL < us);
        us -= delta;
        delta = SysTick->LOAD;
    }
    while (delta - SysTick->VAL < us);
}
