#include "led.h"


char led0_status[2]="0";
char led1_status[2]="0";
char led2_status[2]="0";
char led3_status[2]="0";

/**************************************
*函数名称：	qsdk_hw_led_on
*
*作用：	打开指定LED
*
*形参：	num:	LED 编号
*
*返回值：	无
***************************************/
void qsdk_hw_led_on(int num)
{
	switch(num)
	{
		case 0:	
						HAL_GPIO_WritePin(LED0_GPIO_Port,LED0_Pin,GPIO_PIN_RESET);
//						rt_memset(led0_status,0,sizeof(led0_status));
//						rt_sprintf(led0_status,"%d",1);
//						printf("LED0 is open\r\n");
		break;
		case 1:	
						HAL_GPIO_WritePin(LED1_GPIO_Port,LED1_Pin,GPIO_PIN_RESET);
						rt_memset(led1_status,0,sizeof(led1_status));
						rt_sprintf(led1_status,"%d",1);
						printf("LED1 is open\r\n");
		break;
		case 2:	
						HAL_GPIO_WritePin(LED2_GPIO_Port,LED2_Pin,GPIO_PIN_RESET);
						rt_memset(led2_status,0,sizeof(led2_status));
						rt_sprintf(led2_status,"%d",1);
						printf("LED2 is open\r\n");
		break;
		case 3:	
						HAL_GPIO_WritePin(LED3_GPIO_Port,LED3_Pin,GPIO_PIN_RESET);
						rt_memset(led3_status,0,sizeof(led3_status));
						rt_sprintf(led3_status,"%d",1);
						printf("LED3 is open\r\n");
		break;
		default:break;
	}
}
/**************************************
*函数名称：	qsdk_hw_led_off
*
*作用：	关闭指定LED
*
*形参：	num:	LED 编号
*
*返回值：	无
***************************************/
void qsdk_hw_led_off(int num)
{
	switch(num)
	{
		case 0:
						HAL_GPIO_WritePin(LED0_GPIO_Port,LED0_Pin,GPIO_PIN_SET);
//						rt_memset(led0_status,0,sizeof(led0_status));
//						rt_sprintf(led0_status,"%d",0);
//						printf("LED0 is close\r\n");
		break;
		case 1:
						HAL_GPIO_WritePin(LED1_GPIO_Port,LED1_Pin,GPIO_PIN_SET);
						rt_memset(led1_status,0,sizeof(led1_status));
						rt_sprintf(led1_status,"%d",0);
						printf("LED1 is close\r\n");
		break;
		case 2:
						HAL_GPIO_WritePin(LED2_GPIO_Port,LED2_Pin,GPIO_PIN_SET);
						rt_memset(led2_status,0,sizeof(led2_status));
						rt_sprintf(led2_status,"%d",0);
						printf("LED2 is close\r\n");
		break;
		case 3:
						HAL_GPIO_WritePin(LED3_GPIO_Port,LED3_Pin,GPIO_PIN_SET);
						rt_memset(led3_status,0,sizeof(led3_status));
						rt_sprintf(led3_status,"%d",0);
						printf("LED3 is close\r\n");
		break;
		default:break;
	}
}
/**************************************
*函数名称：	qsdk_hw_led_read
*
*作用：	读取指定LED状态
*
*形参：	num:	LED 编号
*
*返回值：	无
***************************************/
void qsdk_hw_led_read(int num)
{
		switch (num)
    {
			case 0:
					rt_memset(led0_status,0,sizeof(led0_status));
					if(HAL_GPIO_ReadPin(LED0_GPIO_Port, LED0_Pin)==SET)
						rt_sprintf(led0_status,"%d",0);
					else	rt_sprintf(led0_status,"%d",1);
					rt_kprintf("led0==%s \r\n",led0_status);
					break;
			case 1:
					rt_memset(led1_status,0,sizeof(led1_status));
					if(HAL_GPIO_ReadPin(LED1_GPIO_Port, LED1_Pin)==SET)
						rt_sprintf(led1_status,"%d",0);
					else	rt_sprintf(led1_status,"%d",1);
					rt_kprintf("led1==%s \r\n",led1_status);
					break;
			case 2:
					rt_memset(led2_status,0,sizeof(led2_status));
					if(HAL_GPIO_ReadPin(LED2_GPIO_Port, LED2_Pin)==SET)
						rt_sprintf(led2_status,"%d",0);
					else	rt_sprintf(led2_status,"%d",1);
					rt_kprintf("led2==%s \r\n",led2_status);
					break;
			case 3:
					rt_memset(led3_status,0,sizeof(led3_status));
					if(HAL_GPIO_ReadPin(LED3_GPIO_Port, LED3_Pin)==SET)
						rt_sprintf(led3_status,"%d",0);
					else	rt_sprintf(led3_status,"%d",1);
					rt_kprintf("led3==%s \r\n",led3_status);
					break;
			default:
					break;
    }
}


