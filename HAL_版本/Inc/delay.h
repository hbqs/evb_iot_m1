#ifndef	__DELAY_H__

#define __DELAY_H__
#include "rtthread.h"

void HAL_Delay_us(int nus);
void rt_hw_us_delay(rt_uint32_t us);
#endif
