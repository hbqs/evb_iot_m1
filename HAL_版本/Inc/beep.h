#ifndef	__BEEP_H__

#define	__BEEP_H__

void qsdk_hw_beep_on(void);
void qsdk_hw_beep_off(void);

#endif
