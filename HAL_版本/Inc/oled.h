#ifndef _OLED_H_
#define _OLED_H_

extern const unsigned char Qi[];
extern const unsigned char Shi[];
extern const unsigned char Zhi[];
extern const unsigned char Neng[];
extern const unsigned char Ke[];
extern const unsigned char Ji[];
extern const unsigned char wen[];
extern const unsigned char shi[];
extern const unsigned char du[];

#define OLED_ADDRESS	0x78



void oled_init(void);

void oled_send_byte(unsigned char byte);

_Bool oled_write_data(unsigned char byte);

_Bool oled_write_cmd(unsigned char cmd);

void oled_set_address(unsigned char page, unsigned char column);

void oled_clear_screen(void);

void oled_clear_line(unsigned char x);

void oled_dis_128x64_picture(const unsigned char *dp);

void oled_dis_16x16_char(unsigned short page, unsigned short column, const unsigned char *dp);

void oled_dis_6x8_string(unsigned char x, unsigned char y, char *fmt, ...);

void oled_dis_8x16_string(unsigned char x, unsigned char y, char *fmt, ...);


#endif
