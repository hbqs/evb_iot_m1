#ifndef _I2C_H_
#define _I2C_H_

#define IIC_OK		0
#define IIC_Err		1



//SDA		PB11
//SCL		PB10
#if 1
#define SDA_H	HAL_GPIO_WritePin(I2C_SDA_GPIO_Port, I2C_SDA_Pin,GPIO_PIN_SET)
#define SDA_L	HAL_GPIO_WritePin(I2C_SDA_GPIO_Port, I2C_SDA_Pin,GPIO_PIN_RESET)
#define SDA_R	HAL_GPIO_ReadPin(I2C_SDA_GPIO_Port, I2C_SDA_Pin)

#define SCL_H	HAL_GPIO_WritePin(I2C_SCL_GPIO_Port, I2C_SCL_Pin,GPIO_PIN_SET)
#define SCL_L	HAL_GPIO_WritePin(I2C_SCL_GPIO_Port, I2C_SCL_Pin,GPIO_PIN_RESET)

#else 
#define SDA_H	HAL_GPIO_WritePin(I2C_SCL_GPIO_Port, I2C_SCL_Pin,GPIO_PIN_SET)
#define SDA_L	HAL_GPIO_WritePin(I2C_SCL_GPIO_Port, I2C_SCL_Pin,GPIO_PIN_RESET)
#define SDA_R	HAL_GPIO_ReadPin(I2C_SCL_GPIO_Port, I2C_SCL_Pin)

#define SCL_H	HAL_GPIO_WritePin(I2C_SDA_GPIO_Port, I2C_SDA_Pin,GPIO_PIN_SET)
#define SCL_L	HAL_GPIO_WritePin(I2C_SDA_GPIO_Port, I2C_SDA_Pin,GPIO_PIN_RESET)

#endif
typedef struct
{

	unsigned short speed;

} IIC_INFO;

extern IIC_INFO iicInfo;




void rt_hw_i2c_init(void);

void i2c_speed_set(unsigned short speed);

_Bool i2c_write_byte(unsigned char slaveAddr, unsigned char regAddr, unsigned char *byte);

_Bool i2c_read_byte(unsigned char slaveAddr, unsigned char regAddr, unsigned char *val);

_Bool i2c_write_bytes(unsigned char slaveAddr, unsigned char regAddr, unsigned char *buf, unsigned char num);

_Bool i2c_read_bytes(unsigned char slaveAddr, unsigned char regAddr, unsigned char *buf, unsigned char num);

void i2c_start(void);

void i2c_stop(void);

_Bool i2c_wait_ack(unsigned int timeOut);

void i2c_ack(void);

void i2c_no_ack(void);

void i2c_send_byte(unsigned char byte);

unsigned char i2c_recv_byte(void);


#endif
