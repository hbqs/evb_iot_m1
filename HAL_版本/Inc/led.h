#ifndef __LED_H__

#define __LED_H__
#include "main.h"
#include "rtthread.h"

void qsdk_hw_led_on(int num);
void qsdk_hw_led_off(int num);
void qsdk_hw_led_read(int num);
#endif
