/*
 * File      : qsdk_config.c
 * This file is part of config in qsdk
 * Copyright (c) 2018-2030, longmain Development Team
 *
 * Change Logs:
 * Date           Author       Notes
 * 2018-11-14     longmain     first version
 */

#ifndef __QSDK_CONFIG_H__

#define __QSDK_CONFIG_H__

/*********************************
*	NB-IOT模块以及功能选择
*********************************/


#define QSDK_UART  "uart2"


/******  NB-IOT 模块波特率选择  *******/
#define QSDK_UART_BOUND        BAUD_RATE_9600
//#define QSDK_UART_BOUND        BAUD_RATE_115200

/******  启用  M5310 模块  *******/
//#define QSDK_USING_M5310

/******  启用  M5310A 模块  *******/
//#define QSDK_USING_M5310A

/******  启用  ME3616 模块  *******/
#define QSDK_USING_ME3616

#define QSDK_ME3616_LIGHT_ON  //开启ME3616网络指示灯

#define QSDK_USING_ME3616_GPS //开启GPS支持  仅限ME3616-G1A模块




/*********************************
*
*	ONENET 配置选项
*
*********************************/
//开启ONENET支持功能
#define QSDK_USING_ONENET

//定义objectID最大数量
#define QSDK_MAX_OBJECT_COUNT		10

//onenet服务器配置信息
#ifdef QSDK_USING_ME3616
#define QSDK_ONENET_ADDRESS      "183.230.40.39"
#else
#define QSDK_ONENET_ADDRESS      "183.230.40.40"
#endif
#define QSDK_ONENET_PORT			"5683"

/*********************************
*
*	IOT配置选项
*
*********************************/

//开启电信IOT支持功能
//#define QSDK_USING_IOT
#ifdef QSDK_USING_IOT
#define QSDK_IOT_ADDRESS      "117.60.157.137"
#else
#define QSDK_IOT_ADDRESS      "0.0.0.0"
#endif
#define QSDK_IOT_PORT					"5683"

//ME3616注册超时时间
#define QSDK_IOT_REG_TIME_OUT			90

/*********************************
*
*	NET配置选项
*
*********************************/
//开启UDP/TCP功能
//#define QSDK_USING_NET

/*********************************
*
*	SYS配置选项
*
*********************************/
//开启LOG显示
#define QSDK_USING_LOG

//开启调试模式
#define QSDK_USING_DBUG

//定义TCP/UDP最大接收长度
#define QSDK_NET_REV_MAX_LEN		512

//定义模组最大发送数据长度
#define QSDK_CMD_REV_MAX_LEN		1000

//定义时区差
#define QSDK_TIME_ZONE  8


#endif	//qsdk_config.h end


