/*
 * File      : qsdk_callback.c
 * This file is part of callback in qsdk
 * Copyright (c) 2018-2030, longmain Development Team
 *
 * Change Logs:
 * Date           Author       Notes
 * 2018-11-14     longmain     first version
 * 2018-12-13     longmain     update fun
 */

#include "qsdk_callback.h"
#include "stdio.h"
#include "sht20.h"
#include "led.h"
#include "stdlib.h"
#include "rtc.h"
#include "oled.h"

extern char temp[10];
extern char hump[10];
extern RTC_TimeTypeDef RTC_TimeStructure;	
extern RTC_DateTypeDef RTC_DateStructure;

/****************************************************
* 函数名称： qsdk_rtc_set_time_callback
*
* 函数作用： RTC设置回调函数
*
* 入口参数： year：年份		month: 月份		day: 日期
*
*							hour: 小时		min: 分钟		sec: 秒		week: 星期
*
* 返回值： 0 处理成功	1 处理失败
*****************************************************/
void qsdk_rtc_set_time_callback(int year,char month,char day,char hour,char min,char sec,char week)
{ 
	//设置RTC时间函数
	RTC_TimeStructure.Hours=hour;
	RTC_TimeStructure.Minutes=min;
	RTC_TimeStructure.Seconds=sec;
	RTC_DateStructure.Year=year;
	RTC_DateStructure.Month=month;
	RTC_DateStructure.Date=day;
	RTC_DateStructure.WeekDay=week;
	
	HAL_RTC_SetTime(&hrtc,&RTC_TimeStructure,RTC_FORMAT_BIN);
	HAL_RTC_SetDate(&hrtc,&RTC_DateStructure,RTC_FORMAT_BIN);


}
/****************************************************
* 函数名称： qsdk_net_data_callback
*
* 函数作用： TCP/UDP 服务器下发数据回调函数
*
* 入口参数： socket：下发数据的端口号
*
*							data: 数据首地址
*
*							len: 数据长度
*
* 返回值： 0 处理成功	1 处理失败
*****************************************************/
int qsdk_net_data_callback(int socket,char *data,int len)
{
	printf("enter net callback\r\n");
	printf("rev data=%d,%s\r\n",len,data);
	return RT_EOK;
}
/****************************************************
* 函数名称： qsdk_gps_callback
*
* 函数作用： GPS定位成功后回调函数
*
* 入口参数： lon： 经度
*
*							lat: 纬度
*
*							speed: 速度
*
* 返回值： 0 处理成功	1 处理失败
*****************************************************/
int qsdk_gps_callback(double lon,double lat,double speed)
{
	char i;
	printf("lon=%f,lat=%f,speed=%f\r\n",lon,lat,speed);
	
#ifdef QSDK_USING_ME3616
#ifdef QSDK_USING_ME3616_GPS	
	
//此处是从数据流中寻找 lat  lon的分组并且加以修改，可以直接修改main.c 的lat_value   lon_value   字符串
	for(i=0;i<data_stream.dev_len;i++)
	{
		if(data_stream.dev[i].objectid==gps_objectid&&data_stream.dev[i].resourceid==lat_resourceid)
		{
			printf("now lat gps=%s   get=%f",data_stream.dev[i].value,lat);
			rt_memset(data_stream.dev[i].value,0,sizeof(data_stream.dev[i].value));
			sprintf(data_stream.dev[i].value,"%f",lat);
			printf("now lat gps=%s\r\n",data_stream.dev[i].value);
		}
		if(data_stream.dev[i].objectid==gps_objectid&&data_stream.dev[i].resourceid==lon_resourceid)
		{
			printf("now lat gps=%s   get=%f",data_stream.dev[i].value,lon);
			rt_memset(data_stream.dev[i].value,0,sizeof(data_stream.dev[i].value));
			sprintf(data_stream.dev[i].value,"%f",lon);
			printf("now lat gps=%s\r\n",data_stream.dev[i].value);
		}
	}
#endif
#endif
//定位成功后  关闭GPS定位
	if(qsdk_gps_stop()!=RT_EOK)
		rt_kprintf("GPS STOP ERROR\r\n");
	return RT_EOK;
}
/****************************************************
* 函数名称： qsdk_iot_data_callback
*
* 函数作用： IOT平台下发数据回调函数
*
* 入口参数： data：下发数据首地址
*
*							len	:	下发数据长度
*
* 返回值： 0 处理成功	1 处理失败
*****************************************************/
int qsdk_iot_data_callback(char *data,int len)
{
	printf("enter iot callback\r\n");
	printf("rev data=%d,%s\r\n",len,data);
	return RT_EOK;
}
#ifdef QSDK_USING_ONENET
int qsdk_onenet_open_callback()
{
	printf("enter open onenet callback\r\n");

	return RT_EOK;


}
/****************************************************
* 函数名称： qsdk_onenet_close_callback
*
* 函数作用： onenet平台强制断开连接回调函数
*
* 入口参数： 无
*
* 返回值： 0 处理成功	1 处理失败
*****************************************************/
int qsdk_onenet_close_callback()
{
	printf("enter close onenent callback\r\n");

	return RT_EOK;
}
/****************************************************
* 函数名称： qsdk_onenet_read_rsp_callback
*
* 函数作用： onenet平台 read操作回调函数
*
* 入口参数： objectid：需要读取的 object 编号
*
*							instanceid:	需要读取的 instance 编号
*
*							resourceid:	需要读取的 resource 编号
*
* 返回值： 0 处理成功	1 处理失败
*****************************************************/
int qsdk_onenet_read_rsp_callback(int objectid,int instanceid,int resourceid)
{
	printf("enter read dsp callback\r\n");
//	if(objectid==temp_objectid)
//	{
//		sht20_get_value();
//		sprintf(temp,"%0.2f",sht20Info.tempreture);
//		sprintf(hump,"%0.2f",sht20Info.humidity);
//	}
//	else if(objectid==hump_objectid)
//	{
//		sht20_get_value();
//		sprintf(temp,"%0.2f",sht20Info.tempreture);
//		sprintf(hump,"%0.2f",sht20Info.humidity);
//	}
	 if(objectid==light0_objectid&&instanceid==light0_instanceid&&resourceid==light0_resourceid)
		{
			qsdk_hw_led_read(1);
		}
		else if(objectid==light1_objectid&&instanceid==light1_instanceid&&resourceid==light1_resourceid)
		{
			qsdk_hw_led_read(2);
		}
		else if(objectid==light2_objectid&&instanceid==light2_instanceid&&resourceid==light2_resourceid)
		{
			qsdk_hw_led_read(3);
		}
		return RT_EOK;
}
/****************************************************
* 函数名称： qsdk_onenet_write_rsp_callback
*
* 函数作用： onenet平台 write操作回调函数
*
* 入口参数： objectid：需要写入的 object 编号
*
*							instanceid:	需要写入的 instance 编号
*
*							resourceid:	需要写入的 resource 编号
*
*							len:	需要写入的数据长度
*
*							value:	需要写入的数据内容
*
* 返回值： 0 处理成功	1 处理失败
*****************************************************/
int qsdk_onenet_write_rsp_callback(int objectid,int instanceid,int resourceid,int len,char* value)
{
	printf("enter write dsp callback \r\n");
	printf("value=%s \r\n",value);
	if(objectid==light0_objectid&&instanceid==light0_instanceid&&resourceid==light0_resourceid)
	{
		if(atoi(value))
		qsdk_hw_led_on(1);
		else qsdk_hw_led_off(1);
		data_stream.update_status=1;
		qsdk_onenet_notify_data_to_sdk(objectid,instanceid,resourceid);
	}
	else if(objectid==light1_objectid&&instanceid==light1_instanceid&&resourceid==light1_resourceid)
	{
		if(atoi(value))
		qsdk_hw_led_on(2);
		else qsdk_hw_led_off(2);
		data_stream.update_status=1;
		qsdk_onenet_notify_data_to_sdk(objectid,instanceid,resourceid);
	}
	else if(objectid==light2_objectid&&instanceid==light2_instanceid&&resourceid==light2_resourceid)
	{
		if(atoi(value))
		qsdk_hw_led_on(3);
		else qsdk_hw_led_off(3);
		data_stream.update_status=1;
		qsdk_onenet_notify_data_to_sdk(objectid,instanceid,resourceid);
	}
	return RT_EOK;
}
/****************************************************
* 函数名称： qsdk_onenet_exec_rsp_callback
*
* 函数作用： onenet平台 exec操作回调函数
*
* 入口参数： objectid：平台exec命令下发 object 编号
*
*							instanceid:	平台exec命令下发 instance 编号
*
*							resourceid:	平台exec命令下发 resource 编号
*
*							len:	平台exec命令下发数据长度
*
*							cmd:	平台exec命令下发数据内容
*
* 返回值： 0 处理成功	1 处理失败
*****************************************************/
int qsdk_onenet_exec_rsp_callback(int objectid,int instanceid,int resourceid,int len,char* cmd)
{

	printf("enter exec dsp callback\r\n");
	
	printf("exec data len:%d   data=%s\r\n",len,cmd);
	return RT_EOK;

}
#endif

/****************************************************
* 函数名称： qsdk_onenet_exec_rsp_callback
*
* 函数作用： onenet平台 exec操作回调函数
*
* 入口参数： objectid：平台exec命令下发 object 编号
*
*							instanceid:	平台exec命令下发 instance 编号
*
*							resourceid:	平台exec命令下发 resource 编号
*
*							len:	平台exec命令下发数据长度
*
*							cmd:	平台exec命令下发数据内容
*
* 返回值： 0 处理成功	1 处理失败
*****************************************************/
int reboot_callback()
{
	printf("enter reboot callback\r\n");

	return RT_EOK;


}





