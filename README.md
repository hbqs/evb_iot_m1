# 骑士智能科技 EVB_IOT_M1开发板程序

#### 项目介绍
骑士智能科技EVB_IOT_M1开发板程序，其中包含按键、LED、蜂鸣器、SHT20温湿度传感器、OLED显示屏和NB-IOT模组驱动程序，目前程序可以连接ONENET、电信IOT平台、UDP/TCP服务器，更多的程序在更新

#### 软件架构
本程序是基于STM32L151系列单片机编写，采用RT-Thread开源物联网操作系统，可以支持M5310/M5310-A、ME3616等NB-IOT模块，目前已经可以连接骑士云服务器（基于电信IOT）、中移动ONENET、UDP/TCP服务器。

#### 许可证

EVB_IOT_M1开发板程序是一个以Apache许可证2.0版本授权的开源软件，许可证信息以及版权信息一般的可以在代码首部看到：


    /*
    * Copyright (c) 2018-2030, longmain Development Team
    *
    * SPDX-License-Identifier: Apache-2.0
    */

#### 安装教程

1. 下载代码
2. 通过qsdk_config.h修改支持的模块和需要开启的功能。
3. 编译代码并下载

#### 使用说明

1. 代码中支持的模组型号和平台类型均在qsdk_config.h里面定义
2. 如果想修改RT-THREAD嵌入式系统功能，请在rt_config.h里面修改
3. main.c只负责任务运行和基本程序处理

#### 关于技术支持

目前我们骑士智能科技VIP群有350人以上，还有其他分支学习群，我们的开发板都有技术支持

技术支持：程序使用中有故障可以发邮件到我们的邮箱：longmain@longmain.cn

#### 参与贡献

感谢骑士智能科技的支持者和为我们代码实现提供参考意见的开发者